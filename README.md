# maim

screen shot



x11/maim - The NetBSD Packages Collection
[ Back to category | List of categories | List all packages ]

Screenshot utility with improvements over scrot
maim (Make Image) is a utility that takes screenshots of your
desktop. It's meant to overcome shortcomings of scrot and performs
better in several ways.


1649ef309c7f1b0500bf7b2e59ef898dd2259f12ba45022a2f8599f67f2ee75b  1640896309-1-maim-master.zip
075090fd4c3b87663a26d0c2de63b5c822fd96aaeef6ac4fe6ded3abd506be9e  1640896851-1-maim-5.7.4.tar.gz




https://ftp.netbsd.org/pub/pkgsrc/current/pkgsrc/x11/maim/index.html
 


https://www.freshports.org/graphics/maim





````
 maim Screenshot utility with performance improvements over scrot
5.7.4_2 graphics on this many watch lists=1 search for ports that depend on this port Find issues related to this port Report an issue related to this port View this port on Repology. pkg-fallout 5.7.4
Maintainer: 0mp@FreeBSD.org search for ports maintained by this maintainer
Port Added: 2014-11-04 17:30:35
Last Update: 2021-10-28 16:37:30
Commit Hash: f34ece3
People watching this port, also watch:: python, unoconv, py38-pycparser, sysinfo
License: GPLv3
Description:
maim (make image) takes screenshots of your desktop. maim is an
improvement on Scrot, the minimalistic command line screen capturing
application.

WWW: https://github.com/naelstrof/maim
SVNWeb : git : Homepage
pkg-plist: as obtained via: make generate-plist
Expand this list (5 items)
Dependency lines:
maim>0:graphics/maim
To install the port:
cd /usr/ports/graphics/maim/ && make install clean
To add the package, run one of these commands:
pkg install graphics/maim
pkg install maim
PKGNAME: maim
Flavors: there is no flavor information for this port.
distinfo:
TIMESTAMP = 1624533000
SHA256 (naelstrof-maim-v5.7.4_GH0.tar.gz) = 075090fd4c3b87663a26d0c2de63b5c822fd96aaeef6ac4fe6ded3abd506be9e
SIZE (naelstrof-maim-v5.7.4_GH0.tar.gz) = 41663
````



